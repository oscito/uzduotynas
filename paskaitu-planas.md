### LT02V (Šeimyniškių g.)
#### 2017-03-15
Duration of class: **3.5 h**

| Duration | Objective                                 |
|----------|-------------------------------------------|
| ~1 h     | Refresh of skills learned last time       |
| ~1 h     | Finish guestbook with MySQL               |
| ~1.5 h   | Theory about SQL JOINs and practice       |

### 2017-03-15
Duration of class: **3.5 h**

| Duration | Objective                                         |
|----------|---------------------------------------------------|
| ~1 h     | Refresh of skills: creating table, SELECT, INSERT |
| ~1 h     | Theory about SQL JOINs                            |
| ~1.5 h   | SQL JOINs practice                                |

### 2017-03-24
Duration of class: **3.5 h**

| Duration | Objective                       |
|----------|---------------------------------|
| ~1 h     | Refresh of skills               |
| ~1 h     | LIMIT and OFFSET theory         |
| ~1.5 h   | Making movie database paginated |