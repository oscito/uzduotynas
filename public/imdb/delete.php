<?php
include 'helpers.php';
if (!array_key_exists('id', $_GET)) {
	redirectToIndex();
}
$id = $_GET['id'];
deleteMovie($id);
redirectToIndex();