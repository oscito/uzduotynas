<?php include 'header.view.php' ?>
	
	<main class="container">
		<section class="movies" id="movies">
			<h1>Filmas: <?php echo $movie['title']?></h1>
			<hr />
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-6">
					<article class="card">
						<div class="card-block">
							<div class="img-card">
                                <!-- parašome, kad rodytų paveikslėlį iš uploads direktorijos -->
								<img src="uploads/<?=$movie['image']?>" alt="<?=$movie['title']?>" class="img-responsive" />
							</div>
							<p class="tagline card-text text-xs-center">
								Metai: <?php echo $movie['year'] ?> m., Trukmė: <?=$movie['length'] ?>m
							</p>
							<a href="edit.php?id=<?php echo $movie['id']?>" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-pencil"></i> Redaguoti</a>
							<a href="delete.php?id=<?php echo $movie['id']?>" class="btn btn-danger btn-block"><i class="glyphicon glyphicon-trash"></i> Delete</a>
						</div>
					</article>
				</div>
				
				<p><?php echo htmlspecialchars($movie['description']) ?></p>
				
				<iframe width="560" height="315" src="<?php echo str_replace('watch?v=', 'embed/', $movie['trailer']); ?>" frameborder="0" allowfullscreen></iframe>
			</div>
		</section>
	</main>

<?php include 'footer.view.php' ?>