<?php include 'header.view.php' ?>
	
	<main class="container">
        <form action="search.php" method="post">
            <input type="text" name="search" placeholder="Filmo pavadinimas">
            <input type="submit">
        </form>
		<section class="movies" id="movies">
			<h2>Movies</h2>
			<?php foreach($rowset as $movies): ?>
				<div class="row">
					<?php foreach ($movies as $movie): ?>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<article class="card">
								<header class="title-header">
									<h3><?php echo $movie['title'] ?></h3>
								</header>
								<div class="card-block">
									<div class="img-card">
										<img src="<?=$movie['image']?>" alt="<?=$movie['title']?>" class="img-responsive" />
									</div>
									<p class="tagline card-text text-xs-center">
										Metai: <?php echo $movie['year'] ?> m., Trukmė: <?=$movie['length'] ?>m
									</p>
									<a href="show.php?id=<?php echo $movie['id']?>" class="btn btn-primary btn-block"><i class="fa fa-eye"></i> Peržiūrėti</a>
								</div>
							</article>
						</div>
					<?php endforeach ?>
				</div>
			<?php endforeach ?>
		</section>
		<?php showPagination(); ?>
	</main>

<?php include 'footer.view.php' ?>