<?php

include 'helpers.php';

$search = $_POST['search'];
$movie = getMovieByName($search);

if ($movie !== false) {
	redirectToShow($movie['id']);
} else {
	redirectToIndex();
}
