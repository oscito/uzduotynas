<?php
include 'helpers.php';

$page = 1;

if (array_key_exists('page', $_GET)) {
	$page = $_GET['page'];
}

if ($page > countPages()) {
	redirectToIndex();
}

$movies = getMovies($page);
$rowset = array_chunk($movies, 3);
include 'index.view.php';