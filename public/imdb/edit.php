<?php
include 'helpers.php';
if (!array_key_exists('id', $_GET)) {
	redirectToIndex();
}
$id = $_GET['id'];
// patikrina ar formoje yra perduotų duomenų
if (count($_POST) > 0) {
	$image = $_FILES['image'];
	// darom prielaidą, kad nebus nuotraukos
	$photo = null;
	
	// tikrinam, ar failas sėkmingai įkeltas
	// klaidų kodai: http://php.net/manual/en/features.file-upload.errors.php
	if ($image['error'] == 0) {
		// perkeliam failą
		move_uploaded_file( $image['tmp_name'], 'uploads/' . $image['name'] );
		
		// pasižymim įkeltos nuotraukos failo pavadinimą
		$photo = $image['name'];
	}
	
	updateMovie($_POST, $id, $photo);
	redirectToShow($id);
}
$movie = getMovie($id);
if ($movie == false) {
	redirectToIndex();
}
$buttonName = 'Update';
include 'edit.view.php';