<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

// prisijungimas prie duomenų bazės
function createConnection()
{
	// Duomenų bazės kintamieji
	$dsn = 'mysql:dbname=3wa_movies;host=127.0.0.1;charset=utf8';
	$user = 'root';
	$password = 'root';
	// prisijungimas į DB
	$pdo = new PDO($dsn, $user, $password);
	return $pdo;
}

function getMovie($id)
{
	$pdo = createConnection();
	$query = $pdo->prepare("SELECT * FROM movies WHERE id = :id LIMIT 1");
	$query->execute(['id' => $id]);
	/*
		kitas priskyrimo būdas, nurodant duomenų tipą
	$query->bindValue(':id', $id, PDO::PARAM_INT);
	$query->execute();
	*/
	$movie = $query->fetch();
	return $movie;
}

function getMovieByName($name) {
	$pdo = createConnection();
	
	// paimam filmą pagal pavadinimą
	$query = $pdo->prepare("SELECT * FROM movies WHERE title = :title");
	$query->execute([
		'title' => $name
	]);
	
	$result = $query->fetch();
	
	return $result;
}

function redirectToIndex()
{
	header('Location: index.php');
	exit;
}

function redirectToShow($id)
{
	// nukreipiama į to įrašo peržiūros puslapį
	header('Location: show.php?id=' . $id);
	exit;
}

function getMovies($page)
{
	$perPage = 2; // elementų kiekis puslapyje
	$from = ($page - 1) * $perPage; // (n-1)*p
	
	$pdo = createConnection();
	// SQL užklausa
	$query = $pdo->prepare("SELECT * FROM movies LIMIT :from, :amount");
	$query->bindValue(':from', $from, PDO::PARAM_INT);
	$query->bindValue(':amount', $perPage, PDO::PARAM_INT);
	$query->execute();
	
	// SQL užklausos įvykdymas
	return $query->fetchAll();
}

function countMovies() {
	$pdo = createConnection();
	
	// suskaiciuojam, kiek yra duombazėj įrašų
	$query = $pdo->query("SELECT COUNT(*) FROM `movies`");
	$query->execute();
	
	// pasiimam užklausos rezultatą
	$result = $query->fetch();
	// pasiimam tik įrašų skaičių ir verčiam jį skaičiumi (iš stringo)
	$count = (int) $result[0];
	
	return $count;
}

function countPages() {
	$amount = countMovies();
	$perPage = 2;
	// suskaičiuojam puslapių skaičių
	$pages = ceil($amount/$perPage);
	
	return $pages;
}

function showPagination() {
	$pages = countPages();
	
	for ($i = 1; $i <= $pages; $i ++) {
		echo '<a href="?page='. $i .'">'. $i .'</a> ';
	}
}

function createMovie($title, $year, $length, $image, $trailer, $description)
{
	$pdo = createConnection();
	$query = $pdo->prepare("INSERT INTO movies SET title = :title, year = :year, length = :length, image = :image, trailer = :trailer, description = :description;");
	// kadangi masyvas sutampa su tokiu, kurį noriu suformuoti, galiu iš karto perduoti POST
	$query->execute([
		'title' => $title,
		'year' => $year,
		'length' => $length,
		'image' => $image,
		'trailer' => $trailer,
		'description' => $description
	]);
	return $pdo->lastInsertId();
}

function updateMovie($row, $id, $photo)
{
	$update = array_merge($row, ['id' => $id]);
	$pdo = createConnection();
	
	// patikrinam, ar buvo atsiųstas nuotraukos failo pavadinimas, ar ne
	if ($photo != null) {
		// jei buvo atsiųstas nuotraukos failo pavadinimas, tai:
		// * nurodome užklausoje image lauką, kuriame saugosime atsiųsto failo pavadinimas
		$sql = 'UPDATE movies SET title = :title, year = :year, length = :length, trailer = :trailer, description = :description, image = :image WHERE id = :id';
		// * prijungiame prie duomenų patį nuotraukos failo pavadinimą
		$update = array_merge($update, ['image' => $photo]);
		
		$query = $pdo->prepare($sql);
		$query->execute($update);
	} else {
		// neturime failo pavadinimo, tai jį tiesiog praleidžiame
		$sql = "UPDATE movies SET title = :title, year = :year, length = :length, trailer = :trailer, description = :description WHERE id = :id";
		
		$query = $pdo->prepare($sql);
		$query->execute($update);
	}
}

function deleteMovie($id)
{
	// SQL Delete
	$pdo = createConnection();
	$query = $pdo->prepare("DELETE FROM movies WHERE id = :id");
	$result = $query->execute(['id' => $id]);
	return $result;
}