<!-- nurodome, kad formoj bus siunčiami failai -->
<form method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label>Title</label>
		<input type="text" value="<?php echo (isset($movie['title']) ? $movie['title'] : '') ?>" class="form-control" name="title" placeholder="Title" required="true" />
	</div>
	<div class="form-group">
		<label>Year</label>
		<input type="number" value="<?php echo (isset($movie['year']) ? $movie['year'] : '') ?>" class="form-control" name="year" placeholder="Year" required="true" />
	</div>
	<div class="form-group">
		<label>Length</label>
		<input type="number" value="<?php echo (isset($movie['length']) ? $movie['length'] : '') ?>" class="form-control" name="length" placeholder="Length" required="true" />
	</div>
	<div class="form-group">
		<label>Image</label>
        <!-- patikriname, ar yra nuotrauka. jei taip - rodome -->
        <?php if (isset($movie['image'])): ?>
            <img src="uploads/<?php echo $movie['image']; ?>" alt="">
        <?php endif; ?>
        <!-- norint įkelti failą, tai darome per input'ą, kurio type yra "file" -->
        <input type="file" name="image">
	</div>
	
	<div class="form-group">
		<label>Trailer URL</label>
		<input type="text" value="<?php echo (isset($movie['trailer']) ? $movie['trailer'] : '') ?>" class="form-control" name="trailer" placeholder="Youtube url" required="true" />
	</div>
	
	<div class="form-group">
		<label>Description</label>
		<textarea name="description" class="form-control" rows="5" required="true" placeholder="Add information about movie"><?php echo (isset($movie['description']) ? $movie['description'] : '') ?></textarea>
	</div>
	
	<button type="submit" class="btn btn-primary"><?=$buttonName?></button>
</form>