<?php
include 'helpers.php';
if (!array_key_exists('id', $_GET)) {
	header('Location: index.php');
	exit;
}
$id = $_GET['id'];
$movie = getMovie($id);
if ($movie == false) {
	header('Location: index.php');
	exit;
}
include 'show.view.php';