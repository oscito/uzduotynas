<?php
include 'helpers.php';

// patikrina ar formoje yra perduotų duomenų
if (count($_POST) > 0) {
	// pasiimam failą.
	// masyvo index'as (šiuo atveju image) turi sutapt su input[type=file]
	$image = $_FILES['image'];
	// perkeliam failą iš laikinos atminties į uploads direktoriją
	// var_dump($image) // <- pažiūrėkit, kas per velnias guli $image masyve
	move_uploaded_file($image['tmp_name'], 'uploads/'. $image['name']);
	
	// perduodam createMovie funkcijai failo pavadinimą
	$id = createMovie($_POST['title'], $_POST['year'], $_POST['length'], $image['name'], $_POST['trailer'], $_POST['description']);
	// gaunamas paskutinio įterpto įrašo id
	redirectToShow($id);
}

$buttonName = 'Create';
include 'create.view.php';