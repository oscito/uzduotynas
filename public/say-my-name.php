<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
	<div style="margin: 0 auto; text-align: center;">
		<?php
		
		if (array_key_exists('name', $_POST)) {
			$name = $_POST['name'];
			
			if (strlen($name) == 0) {
				echo 'Tell me your name please, don\'t be so shy :)<br>';
			} else {
				echo 'Hi, ' . $name . '<br>';
			}
			echo '<a href="?">One more time?</a>';
		} else {
			echo '<form action="?" method="post">';
			echo '<strong>What is your name?</strong><br>';
			echo '<input type="text" name="name"><br>';
			echo '<input type="submit" value="Confirm!">';
			echo '</form>';
		}
		
		?>
	</div>
</body>
</html>