<?php

require_once 'templates/header.phtml';
require_once 'dictionary.php';

if (array_key_exists('submit', $_POST)) {
	$availableTypes = [
		'en-lt',
		'lt-en'
	];
	
	if (! array_key_exists('word', $_POST) || strlen($_POST['word']) < 1) {
		$error = 'Please enter word';
		require_once 'templates/error.phtml';
	} else if (
		! array_key_exists('type', $_POST) ||
	    ! in_array($_POST['type'], $availableTypes)
	) {
		$error = 'Could not understand how you want to translate';
		require_once 'templates/error.phtml';
	} else {
		$word = $_POST['word'];
		$type = $_POST['type'];
		
		if ($type == 'lt-en') {
			if (! array_key_exists($word, $dictionary)) {
				$error = 'I don\'t know your word. sorry';
				require_once 'templates/error.phtml';
			} else {
				$translation = $dictionary[$word];
				require_once 'templates/translation.phtml';
			}
		} else {
			$translation = array_search($word, $dictionary);
			
			if (! $translation) {
				$error = 'I don\'t know your word. sorry';
				require_once 'templates/error.phtml';
			} else {
				require_once 'templates/translation.phtml';
			}
		}
	}
}

require_once 'templates/form.phtml';
require_once 'templates/dictionary.phtml';
require_once 'templates/footer.phtml';