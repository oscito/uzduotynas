<?php

define('BASE_DIR', dirname(__FILE__) .'/');
define('TEMPLATES_DIR', BASE_DIR .'templates/');
define('INCLUDES_DIR', BASE_DIR .'includes/');
define('PAGES_DIR', BASE_DIR .'pages/');
define('DATA_FILE', BASE_DIR .'data.txt');

require_once INCLUDES_DIR .'get-tasks.php';

$op = 'home';

if (array_key_exists('op', $_GET)) {
	$op = $_GET['op'];
}

$page = '';

switch($op) {
	case 'home':
	case 'new-task':
	case 'done-task':
	case 'delete-task':
	case 'task':
		$page = $op;
		break;
		
	default:
		$page = '404';
		break;
}

$pageScript = PAGES_DIR . $page . '.php';

require_once $pageScript;
require_once INCLUDES_DIR .'save-tasks.php';
