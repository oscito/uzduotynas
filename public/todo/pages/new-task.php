<?php

$title = '';
if (array_key_exists('title', $_POST)) {
	$title = $_POST['title'];
}

$text = '';
if (array_key_exists('text', $_POST)) {
	$text = $_POST['text'];
}

$priority = '';
if (array_key_exists('priority', $_POST)) {
	$priority = $_POST['priority'];
}

$day = '';
if (array_key_exists('day', $_POST)) {
	$day = $_POST['day'];
}

$month = '';
if (array_key_exists('month', $_POST)) {
	$month = $_POST['month'];
}

$year = '';
if (array_key_exists('year', $_POST)) {
	$year = $_POST['year'];
}

$array = [
	'title' => $title,
	'text' => $text,
	'priority' => $priority,
	'deadline' => [
		'day' => $day,
		'month' => $month,
		'year' => $year,
	],
	'status' => 'pending'
];

$tasks[] = $array;

header('Location: index.php');