<?php

if (! array_key_exists('id', $_GET) || !preg_match('/^[0-9]+$/', $_GET['id'])) {
	die('Task not found');
}

$id = (int) $_GET['id'];

require INCLUDES_DIR .'find-task.php';

var_dump($tasks[$id]);

$tasks[$id]['status'] = 'done';

header('Location: index.php?op=task&id='. $id);