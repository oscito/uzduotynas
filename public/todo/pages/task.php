<?php

if (! array_key_exists('id', $_GET) || !preg_match('/^[0-9]+$/', $_GET['id'])) {
	die('Task not found');
}

$id = (int) $_GET['id'];

require INCLUDES_DIR .'find-task.php';
require_once TEMPLATES_DIR .'header.phtml';
require_once TEMPLATES_DIR .'task.phtml';
require_once TEMPLATES_DIR .'footer.phtml';