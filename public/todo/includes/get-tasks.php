<?php

$tasks = [];

$file = fopen(DATA_FILE, 'r');

if (! $file) {
	die('negaliu atidaryti failo');
}

$i = 0;

while (($data = fgetcsv($file)) !== FALSE) {
	$tasks[] = [
		'title' => $data[0],
		'text' => $data[1],
		'priority' => $data[2],
		'deadline' => [
			'day' => $data[3],
			'month' => $data[4],
			'year' => $data[5],
		],
		'status' => $data[6]
	];
	
	$i ++;
}

fclose($file);