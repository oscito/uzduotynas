<?php

$file = fopen(DATA_FILE, 'w');

foreach ($tasks as $task) {
	$data = [
		$task['title'], $task['text'], $task['priority'],
		$task['deadline']['day'], $task['deadline']['month'],
		$task['deadline']['year'], $task['status']
	];
	
	fputcsv($file, $data);
}

fclose($file);
