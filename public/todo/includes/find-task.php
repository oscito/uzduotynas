<?php

if (! array_key_exists($id, $tasks)) {
	die ('Task not found');
}

$task = $tasks[$id];