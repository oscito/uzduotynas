<?php

require_once './uzduotys.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>3WA - Tomas Čerkauskas</title>
    <style>body{margin:1em auto;max-width:40em;padding:0 .62em;font:1.2em/1.62em sans-serif;}h1,h2,h3{line-height:1.2em;}@media print{body{max-width:none}}</style>
</head>
<body>
    <article>
        <?php
        
        $uzd = array_key_exists('uzd', $_GET) && in_array($_GET['uzd'], array_keys($uzduotys)) ? $_GET['uzd'] : null;
        
        if ($uzd):
            $uzduotis = $uzduotys[$uzd];
            ?>
            <header>
                <p>Grįžti į <a href="index.php">pradžią</a></p>
                <h1><?php echo $uzduotis['pavadinimas']; ?></h1>
                <p><?php echo $uzduotis['aprasymas']; ?></p>
                <?php if (array_key_exists('pvz', $uzduotis)): ?>
                    <p>Pavyzdį galite pažiūrėti <a href="<?php echo $uzduotis['pvz']; ?>">čia</a></p>
                <?php endif; ?>
            </header>
            <?php if (array_key_exists('etapai', $uzduotis)): ?>
                <section>
                    <h2>Užduoties etapai</h2>
                    <ol>
                        <?php foreach ($uzduotis['etapai'] as $etapas): ?>
                            <li><?php echo $etapas; ?></li>
                        <?php endforeach; ?>
                    </ol>
                </section>
            <?php endif; ?>
            <?php if (array_key_exists('zinios', $uzduotis)): ?>
                <section>
                    <h2>Žinios, reikalingos užduočiai atlikti</h2>
                    <ul>
                        <?php foreach ($uzduotis['zinios'] as $zinia): ?>
                            <li>
                                <?php echo $zinia; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </section>
            <?php endif; ?>
            <section>
                <h2>Naudingi dalykai, galintys padėti išspręsti užduotį</h2>
                <?php foreach($uzduotis['naudingi'] as $naudingas): ?>
                    <?php if (array_key_exists('title', $naudingas)): ?>
                        <h3><?php echo $naudingas['title']; ?></h3>
                    <?php endif; ?>
                    <p><?php echo nl2br($naudingas['content']); ?></p>
                <?php endforeach; ?>
            </section>
        <?php else: ?>
            <header>
                <h1>Kaip tapti WEB developer'iu</h1>
            </header>
            <?php foreach ($moduliai as $modulis): ?>
                <section>
                    <header>
                        <h2><?php echo $modulis['pavadinimas']; ?></h2>
                        <p><?php echo $modulis['aprasymas']; ?></p>
                    </header>
                    <article>
                        <h3>Užduotys</h3>
                        <ul>
                            <?php foreach ($modulis['uzduotys'] as $uzduotiesId): ?>
                                <?php if (array_key_exists($uzduotiesId, $uzduotys)): ?>
                                    <?php $uzduotis = $uzduotys[$uzduotiesId]; ?>
                                    <li><a href="?uzd=<?php echo $uzduotiesId; ?>"><?php echo $uzduotis['pavadinimas']; ?></a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </article>
                </section>
            <?php endforeach; ?>
        <?php endif; ?>
    </article>
</body>