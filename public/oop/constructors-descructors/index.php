<!-- Forma, iš kurios siųsime duomenis POST metodu į write.php -->
<form action="write.php" method="post">
    <input type="text" name="author" placeholder="author"><br>
    <textarea name="content" id="" cols="30" rows="10"></textarea><br>
    <input type="submit">
</form>
<?php

// Kadangi mes negalime naudoti klasės jos neincludine, tai includinam
// P.S. šitoj vietoj niekas nevyksta, tiesiog yra nusakoma, kad per
//      kas yra tas PostRepository klasė
require_once 'PostRepository.class.php';

// sukuriame PostRepository klasės objektą
$repo = new PostRepository();

// dabar jau galime kviesti metodą getAll(), kuris gražina masyvą su Post klasės objektais
$posts = $repo->getAll();

foreach ($posts as $post) {
    // pasiimam iš properčių mums aktualius duomenis (t.y. posto autorių ir turinį)
    // ir išvedam
    echo $post->author .': '. $post->content .'<br>';
}