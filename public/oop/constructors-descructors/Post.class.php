<?php

/**
 * Klasės Post paskirtis yra aprašyti objektus, kurie bus tarsi tiltas tarp duomenų bazės įrašo
 * ir mūsų programos. T.y. kiekvienas duomenų bazės įrašas mūsų programoje bus paverstas į Post
 * klasės objektą (vertimą darys PostRepository)
 */
class Post {
    // Atitinka db lentelės stulpelį id
    public $id;

    // Atitinka db lentelės stulpelį author
    public $author;

    // Atitinka db lentelės stulpelį content
    public $content;
}