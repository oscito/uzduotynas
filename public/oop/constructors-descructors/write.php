<?php

// pasiimame duomenis iš formos, aprašytos index.php faile
$author = $_POST['author'];
$content = $_POST['content'];

// Kadangi mums bus reikalinga PostRepository klasė, tai mes ją includinam
require_once 'PostRepository.class.php';

// sukuriamą PostRepository klasės objektą
$repo = new PostRepository();

// Sukuriame Post klasės objektą
$post = new Post();

// į Post klasės objekto properčius "author" ir "content" įrašome duomenis iš formos
$post->author = $author;
$post->content = $content;

// sakome PostRepository klasės objektui, kad pridėtų į duomenų bazę šitą Post'ą, kurį kątik sukūrėme
$repo->add($post);

// siunčiam žmogų atgal į index'ą - jo darbas čia baigtas
header('Location: index.php');