<?php

require 'Post.class.php';

/**
 * PostRepository yra tiltas tarp duomenų bazės lentelės `posts` ir mūsų programos
 * PostRepository dirba tik su Post klasės objektais. T.y. getAll metods gražina
 * masyvą su Post klasės objektais, o add metodas priima vienintelį argumentą,
 * kuris yra Post klasės tipo
 *
 * Atkreipkite dėmesį, kad dėl šios klasės esmės (buvimo tiltu tarp programos ir db),
 * mes niekur daugiau nenaudojame jokių PDO ar prisijungimų prie duomenų bazės, nes
 * tai yra PostRepository klasės darbas
 * T.y. mes efektyviai paslėpėme duomenų bazės logiką nuo biznio logikos
 */
class PostRepository {
    /**
     * Propertis, kuris laiko duomenų bazės prisijungimą, sukuriamą konstruktoriuje
     */
    public $pdo;

    /**
     * PostRepository konstruktorius
     * Šitas metodas yra iškviečiamas visada, kai yra kuriamas šitos klasės objektas
     *
     * Šioje klasėje, konstruktorius per PDO tiesiog prisijungia prie duomenų bazės
     * ir išsaugo prisijungimą propertyje, kurį galėsime naudoti metoduose
     */
    public function __construct()
    {
        // Duomenų bazės kintamieji
        $dsn = 'mysql:dbname=scotchbox;host=127.0.0.1;charset=utf8';
        $user = 'root';
        $password = 'root';

        // prisijungimas į DB
        $this->pdo = new PDO($dsn, $user, $password);
    }

    /**
     * getAll metodo prasmė yra gražinti visus duomenų bazės įrašus
     */
    public function getAll()
    {
        // pasiimame iš properčio PDO objektą (prisijungimą prie db), kurį sukūrėme konstruktoriuje
        $pdo = $this->pdo;
        // užklausa, paimanti visus įrašus iš duomenų bazės
        $query = $pdo->query("SELECT * FROM posts");
        // pasiimame visus įrašus masyvo pavidalu
        $results = $query->fetchAll();

        // toliau esančio kodo esmė yra paversti visus įrašus iš masyvo į Post klasės objektus
        // kiekvienam įrašui kuriame po objektą

        // susikuriame masyvą, į kurį dėsime objektus
        $return = [];

        // bėgame per visus duomenų bazės įrašus masyvo pavidalu
        foreach ($results as $item) {
            // $item masyve turime konkrečius vieno įrašo duomenis iš duomenų bazės
            // sukuriame Post klasės objektą
            $post = new Post();

            // į sukurto objekto properčius įrašome duomenis iš duomenų bazės
            $post->id = $item['id'];
            $post->author = $item['author'];
            $post->content = $item['content'];

            // objektą, su duomenim iš duomenų bazės, įrašome į masyvą
            $return[] = $post;
        }

        // gražiname rezultatą, t.y. Post klasės objektus, su duomenimis iš db
        return $return;
    }

    /**
     * Metodo add prasmė yra įrašyti duomenis iš Post klasės objekto į duomenų bazę
     * Metodas priima vienintelį argumentą - Post klasės objektą, kurio duomenis mes
     * įrašinėsime į duomenų bazę
     */
    public function add($post)
    {
        // pasiimame duomenis iš gauto objekto properčių
        $author = $post->author;
        $content = $post->content;

        // iš properčio pasiimame PDO objektą (prisijungimą prie duomenų bazės)
        $pdo = $this->pdo;
        // paruošiame insert užklausą
        $query = $pdo->prepare("INSERT INTO `posts` SET `author` = :author, `content` = :content");
        // vykdome užklausą su duomenimis, kurie atėjo kartu su objektu
        $query->execute([
            'author' => $author,
            'content' => $content
        ]);
    }
}