<?php

$rezisIki = 100;

if (array_key_exists('iki', $_GET) && preg_match('/^[0-9]*$/',$_GET['iki'])) {
	$rezisIki = (int) $_GET['iki'];
}

$rezisNuo = 0;

if (array_key_exists('nuo', $_GET) && preg_match('/^[0-9]*$/', $_GET['nuo'])) {
	$rezisNuo = (int) $_GET['nuo'];
}

$masyvas = [];

for ($i = $rezisIki; $i > $rezisNuo; $i -= 2) {
	$masyvas[] = $i;
}

$psl = 1;

if (array_key_exists('psl', $_GET) && preg_match('/^[0-9]*$/', $_GET['psl'])) {
	$psl = (int) $_GET['psl'];
}

if ($psl < 1) {
	$psl = 1;
}

$puslapiu = ceil( count( $masyvas ) / 10 );

if ($psl > $puslapiu) {
	$psl = $puslapiu;
}

$nuo = ($psl - 1) * 10;
$iki = $nuo + 10;

for ($i = $nuo; $i < $iki; $i ++) {
	echo $masyvas[$i] .'<br>';
}

echo '<br>';

for ($i = 1; $i <= $puslapiu; $i ++) {
	echo '<a href="?psl='. $i .'&amp;nuo='. $rezisNuo .'&amp;iki='. $rezisIki .'">'. $i .'</a> ';
}