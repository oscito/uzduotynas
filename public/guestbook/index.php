<?php

$op = '';

if (array_key_exists('op', $_GET)) {
	$op = $_GET['op'];
}

$error = null;

if ($op == 'write') {
	$fields = ['author', 'content'];
	
	foreach ($fields as $field) {
		if (! array_key_exists($field, $_POST) || strlen($_POST[$field]) < 1) {
			$error = 'All fields are mandatory!';
		}
	}
	
	if (! $error) {
		$array = [
			$_POST['author'], $_POST['content']
		];
		
		$file = fopen('data.txt', 'a');
		if (! $file) {
			$error = 'Could not open file to save!';
		} else {
			fputcsv( $file, $array );
			fclose( $file );
		}
	}
}

include 'header.php';

if ($error != null) {
	include 'error.php';
}

include 'list.php';
include 'form.php';
include 'footer.php';