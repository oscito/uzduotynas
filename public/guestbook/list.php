<section>
	<h1>What guests say</h1>
<?php

$handle = fopen('data.txt', 'r');

if (! $handle) {
	echo 'Could not open file';
} else {
	while (($data = fgetcsv($handle)) !== FALSE) {
		list($author, $comment) = $data;
		
		echo '<p><b>'. htmlspecialchars($author) .'</b>: '. htmlspecialchars($comment) .'</p>';
	}
	
	fclose($handle);
}

?>
</section>