<?php

$charset = 'utf8';
$host = 'localhost';

//$db   = 'scotchbox';
//$user = 'root';
//$pass = 'root';
$db   = 'guestbook';
$user = '3wa';
$pass = 'slaptazodis';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";

$pdo = new PDO($dsn, $user, $pass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>MySQL guestbook</title>
</head>
<body>

<?php

$op = null;

if (array_key_exists('op', $_GET)) {
	$op = $_GET['op'];
}

if ($op == 'new') {
	$name = '';
	$content = '';
	
	if (array_key_exists('name', $_POST)) {
		$name = $_POST['name'];
	}
	
	if (array_key_exists('content', $_POST)) {
		$content = $_POST['content'];
	}
	
	if ($name != '' && $content != '') {
		$query = $pdo->prepare( "INSERT INTO `posts` SET name = :name, content = :content" );
		$query->execute([
			'name' => $name,
			'content' => $content
		]);
		
		?>
		Ačiū už nuomonę!<br>
		<a href="?">Į Pradžią</a>
		<?php
	} else {
		?>
		Palikai tuščią lauką!<br>
		<a href="?">Į Pradžią</a>
		<?php
	}
} else if ($op == 'delete') {
	if (array_key_exists('id', $_GET)) {
		$query = $pdo->prepare( "DELETE FROM `posts` WHERE id = :id" );
		$query->execute( [
			'id' => $_GET['id']
		] );
	}
	
	?>
	Įrašas ištrintas!<br>
	<a href="?">Į Pradžią</a>
	<?php
} else if ($op == 'edit' || $op == 'update') {
	if (! array_key_exists('id', $_GET)) {
		?>
		Nepavyko rasti nurodyto įrašo. <br>
		<a href="?">Į pradžią</a>
		<?php
	} else {
		$id = $_GET['id'];
		
		$find = $pdo->prepare("SELECT * FROM `posts` WHERE id = :id");
		$find->execute( [
			'id' => $id
		] );
		
		$result = $find->fetchAll();
		
		if (sizeof($result) != 1) {
			?>
			Nepavyko rasti nurodyto įrašo. <br>
			<a href="?">Į pradžią</a>
			<?php
		} else {
			$post = $result[0];
			if ($op == 'edit') {
				?>
				<h2>Redagavimas</h2>
				<form action="?op=update&id=<?php echo $post['id']; ?>" method="post">
					<input type="text" name="name" value="<?php echo htmlspecialchars($post['name']); ?>"><br>
					<textarea name="content" id="" cols="30" rows="10"><?php echo htmlspecialchars($post['content']); ?></textarea><br>
					<input type="submit" value="Saugoti">
				</form>
				<?php
			} else {
				$name = '';
				$content = '';
				
				if (array_key_exists('name', $_POST)) {
					$name = $_POST['name'];
				}
				
				if (array_key_exists('content', $_POST)) {
					$content = $_POST['content'];
				}
				
				if ($name != '' && $content != '') {
					$query = $pdo->prepare('UPDATE `posts` SET name = :name, content = :content WHERE id = :id');
					
					$query->execute([
						'name' => $name,
						'content' => $content,
						'id' => $post['id']
					]);
					?>
					Įrašas atnaujintas.<br>
					<a href="?op=edit&amp;id=<?php echo $post['id']; ?>">Į redagavimą</a> | <a href="?">Į pradžią</a>
					<?php
				} else {
					?>
					Palikote tuščią lauką.<br>
					<a href="?op=edit&amp;id=<?php echo $post['id']; ?>">Į redagavimą</a> | <a href="?">Į pradžią</a>
					<?php
				}
			}
		}
	}
} else {
	$query = $pdo->query("SELECT * FROM `posts` ORDER BY id DESC");
	$posts = $query->fetchAll();
	?>
	<div>
		<?php foreach ($posts as $post): ?>
			<div style="border: 1px solid black">
				<h5><?php echo $post['name']; ?> <a href="?op=delete&amp;id=<?php echo $post['id']; ?>">X</a> <a href="?op=edit&amp;id=<?php echo $post['id']; ?>">E</a></h5>
				<p><?php echo $post['content']; ?></p>
			</div>
		<?php endforeach; ?>
	</div>
	<form action="?op=new" method="post">
		<input type="text" name="name"><br>
		<textarea name="content" id="" cols="30" rows="10"></textarea><br>
		<input type="submit" value="Saugoti">
	</form>
	<?php
}

?>

</body>
</html>
