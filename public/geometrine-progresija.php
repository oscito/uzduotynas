<?php

$pirmasNarys = 1;
$daugiklis = 2;
$nariu = 20;

if (array_key_exists('a', $_GET) && preg_match('/^[0-9]+$/', $_GET['a'])) {
	$pirmasNarys = (int) $_GET['a'];
}

if (array_key_exists('q', $_GET) && preg_match('/^[0-9]+$/', $_GET['q'])) {
	$daugiklis = (int) $_GET['q'];
}

if (array_key_exists('n', $_GET) && preg_match('/^[0-9]+$/', $_GET['n'])) {
	$nariu = (int) $_GET['n'];
}

$suma = $pirmasNarys;
$paskNarys = $pirmasNarys;

for ($i = 1; $i < $nariu; $i ++) {
	$paskNarys = $paskNarys * $daugiklis;
	$suma = $suma + $paskNarys;
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>3WA - Tomas Čerkauskas</title>
	<style>body{margin:1em auto;max-width:40em;padding:0 .62em;font:1.2em/1.62em sans-serif;}h1,h2,h3{line-height:1.2em;}@media print{body{max-width:none}}</style>
</head>
<body>
<article>
	<section>
		<h2>Geometrines progresijos nariu suma</h2>
		<p>
			<b>Pirmasis narys:</b> <?php echo $pirmasNarys; ?><br>
			<b>Daugiklis:</b> <?php echo $daugiklis; ?><br>
			<b>Narių skaičius:</b> <?php echo $nariu; ?><br>
		</p>
		<p><b>Suma:</b> <?php echo $suma; ?></p>
	</section>
	<section>
		<form method="get">
			<input type="number" placeholder="Pirmas narys" name="a">
			<input type="number" placeholder="Daugiklis" name="q">
			<input type="number" placeholder="Narių skaičius" name="n">
			<input type="submit" value="Perskaičiuoti">
		</form>
	</section>
	
</article>
</body>
</html>
