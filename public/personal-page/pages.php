<?php

$contacts = [
	'template' => 'contacts',
	'uri' => 'contacts',
	'title' => 'Kontaktai'
];

$about = [
	'template' => 'about',
	'uri' => 'about',
	'title' => 'Apie mane'
];

$cv = [
	'template' => 'cv',
	'uri' => 'cv',
	'title' => 'Curriculum Vitae'
];

$home = [
	'template' => 'home',
	'title' => 'Namai'
];

$pages = [
	$home, $cv, $about, $contacts
];