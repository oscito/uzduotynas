<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once 'pages.php';

$currentPage = '';

if (array_key_exists('page', $_GET)) {
	$currentPage = $_GET['page'];
}

foreach ($pages as $page) {
	$uri = '';
	
	if (array_key_exists('uri', $page)) {
		$uri = $page['uri'];
	}
	
	if ($uri == $currentPage) {
		$template = 'templates/'. $page['template'] .'.phtml';
		
		require_once $template;
		die;
	}
}

require_once 'templates/404.phtml';