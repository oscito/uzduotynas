<?php

$moduliai = [
	[
		'pavadinimas' => 'PHP1',
		'aprasymas' => 'Šio modulio metu, pristatoma medžiaga ir užduotys apie pagrindinius PHP konceptus/idėjas,'
					   .' darbą su skirtingais PHP failais vienoje programoje, šablonus (angl. template),'
		               .' ciklus, GET ir POST komunikaciją, darbą su failų sistema ir CSV failais.',
		'uzduotys' => [
			'say-my-name',
			'puslapiavimas',
			'geometrine-progresija',
			'vertejas',
			'personal-page',
			'guestbook',
			'todo'
		]
	],
	[
		'pavadinimas' => 'SQL1',
		'aprasymas' => 'Šio modulio metu, pristatoma medžiaga apie reliacines duomenų bazes ir jų struktūrą,'
		               .' phpMyAdmin - MySQL valdymo įrankį, SELECT, INSERT, UPDATE, DELETE užklausas,'
		               .' kaip panaudoti duomenų bazę programuojant su PHP per PHP Data Objects (PDO)',
		'uzduotys' => [
			'simple-select',
			'select-with-where',
			'joins'
		]
	]
];

$uzduotys = [
	'puslapiavimas' => [
		'pavadinimas' => 'Puslapiavimas',
		'aprasymas' => 'Užduoties tikslas išmokti pritaikyti matematines operacijas, ciklus ir masyvus.',
		'pvz' => 'puslapiavimas.php',
		'zinios' => [
			'Kintamieji',
			'Ciklai',
			'Masyvai',
			'Pagrindinės matematinės operacijos (+, -, *, /)',
			'Suvokimas apie $_GET',
			'<i>Papildomai: norintiems padaryti "bullet-proof", kai kuriose vietose reikalingas if\'as.</i>'
		],
		'etapai' => [
			'Susikurti masyva is 100 pirmu lyginiu skaiciu mazejancia tvarka',
			'Padaryti puslapiavima. T.y. adrese, get formatu bus nurodytas puslapio numeris. Rodyti 10 skaiciu per puslapi.',
			'Rodytu puslapius apacioje ir kad paspaudus ant numerio, nuvestu i kita puslapi (pasinaudoti $_GET).',
			'Pasinaudojant $_GET galima butu keisti 1 dalyje nurodytus rezius.'
		],
		'naudingi' => [
			[
				'title' => 'Puslapiavimo logikos pseudokodas',
				'content' => "var masyvas = < lyginiai skaiciai >;
					var puslapis = 1; // ". '$_GET[\'psl\']' . "
					var nuo = (puslapis - 1) * 10;
					var iki = nuo + 10;
					
					ciklas:
					  `reiksme laikysim` i
					  `sukti nuo` nuo
					  `iki` iki
					  `zingsnis` 1
					  {
					      isvedam masyvas[i]
					  }"
			]
		]
	],
	'geometrine-progresija' => [
		'pavadinimas' => 'Geometrines progresijos narių suma',
		'aprasymas' => 'Geometrinė progresija - tai tokia skaičių seka, kurios nariai daugėja progresyviai.
						Pavyzdžiui, jei geometrinės progresijos pirmas narys yra 1, o daugiklis b = 2, bei A<sub>n</sub> yra n-tasis narys, tai
						A<sub>1</sub>=1, A<sub>2</sub>=A<sub>1</sub>*2=2, A<sub>3</sub>=A<sub>2</sub>*2=4, A<sub>4</sub>=A<sub>3</sub>*2=8 ir t.t.
						Raskite pirmųjų n geometrinės progresijos narių sumą (nesinaudokit matematine formule, neįdomu tada bus, bet atsakymui pasitikrint galit :]).',
		'pvz' => 'geometrine-progresija.php',
		'zinios' => [
			'Kintamieji',
			'Ciklai',
			'Pagrindinės matematinės operacijos (+, -, *, /)'
		],
		'etapai' => [
			'Parašyti ciklą, kuris skaičiuotų pirmųjų 20 geometrinės progresijos narių sumą, kai pirmasis narys A<sub>1</sub>=1, daugiklis b = 2.',
			'Padaryti, kad narių skaičių, pirmojo nario ir daugiklio reikšmės būtų galima keisti pasinaudojant $_GET',
			'<i>Papildomai: sukurti formą, kurioje vartotojas galėtų įrašyti visas tris reikšmes į input\'us ir paspaudus submit\'ą, skaičiavimai vyktų pagal įvestus duomenis</i>'
		],
		'naudingi' => [
			[
				'content' => '<a href="https://lt.wikipedia.org/wiki/Geometrin%C4%97_progresija" target="_blank">Geometrinė progresija wikipedijoje</a>'
			]
		]
	],
	'vertejas' => [
		'pavadinimas' => 'Anglų-lietuvių vertėjas',
		'aprasymas' => 'Anglų-lietuvių vertėjas verčia žodžius tarp šių dviejų kalbų (lietuvių-anglų ir anglų-lietuvių). Sakinių versti nereikia'
		               .' (nebent norit parašyt dirbtinį intelektą ;>)',
		'pvz' => 'translator',
		'zinios' => [
			'Kintamieji',
			'Masyvai',
			'Ciklai',
			'Sąlygos kintamasis',
			'GET duomenys',
			'Programos skaidymas į atskirus failus'
		],
		'etapai' => [
			'Susikurti formą žodžiui įvesti ir vertimo krypčiai (lietuvių-anglų/anglų-lietuvių) nustatyti ir šie duomenys būtų perduodami per GET',
			'Susikurti žodyną (masyvas) su abiejų kalbų žodžiais',
			'Padaryti paiešką žodyne pagal vartotojo įvestą žodį viena kryptimi (pvz.: tik lietuvių-anglų)',
			'Padaryti paiešką žodyne kita kryptimi',
			'Apsaugoti programą atvejais, kai žmogus nieko neįvedė arba kai įvedė žodį, kurio žodyne nėra ir parodyti jam pranešimą apie tai',
		],
		'naudingi' => [
			[
				'content' => '<a href="http://www.phpknowhow.com/get-and-post/understanding-get-and-post/" target="_blank">Apie GET ir POST</a>'
			],
			[
				'content' => '<a href="http://www.w3schools.com/php/php_includes.asp" target="_blank">Skaidymas į atskirus failus</a>'
			]
		]
	],
	'personal-page' => [
		'pavadinimas' => 'Asmeninis puslapis',
		'aprasymas' => 'Užduoties tikslas - pasidaryti asmeninį puslapį, kurio turinys būtų lengvai valdomas nekeičiant pačios sistemos veikimo. Duomenys laikomi failuose.',
		'pvz' => 'personal-page',
		'zinios' => [
			'Kintamieji',
			'Masyvai',
			'Ciklai',
			'Sąlygos kintamasis',
			'GET duomenys',
			'Programos skaidymas į atskirus failus'
		],
		'etapai' => [
			'Sugalvoti, kokie puslapiai bus pateikiami vartotojui. Jų turėtų būti 3-5.',
			'Susikurti šabloninius failus (<i>angl. template</i>) headeriui (turi turėti linkus į visus puslapius) ir footeriui.',
			'Susikurti po šabloninį failą kiekvienam puslapiui atskirai (jie turi naudoti header ir footer šabloninius failus).',
			'Sukurti index\'inį failą, kuris reguliuotų kuris template bus rodomas',
			'Padaryti, kad informacija apie puslapius (pavadinimas, adreso identifikatorius, šabloninio failo pavadinimas) būtų saugoma masyve ir index\'e būtų naudojamas tas masyvas atrinkti puslapiui.',
			'Padaryti, kad linkai headeryje būtų generuojami pagal masyvo informaciją',
			'Padaryti 404 puslapį, kuris būtų rodomas jei atidarytas puslapis neegzistuoja.',
			'<i>Papildomai: gebėti įrodyti ir pavaizduoti, kodėl toks variantas yra/nėra geresnis už pirminį.</i>'
		],
		'naudingi' => [
			[
				'content' => '<a href="http://www.phpknowhow.com/get-and-post/understanding-get-and-post/" target="_blank">Apie GET ir POST</a>'
			],
			[
				'content' => '<a href="http://www.w3schools.com/php/php_includes.asp" target="_blank">Skaidymas į atskirus failus</a>'
			]
		]
	],
	'guestbook' => [
		'pavadinimas' => 'Svečių knyga',
		'aprasymas' => 'Svečių knyga, tai viena, kur žmonės palikinėja atsiliepimus apie puslapį. Tikslas: išmokti saugoti duomenis failuose, bei suprasti kaip veikia $_POST',
		'pvz' => 'guestbook',
		'zinios' => [
			'Kintamieji',
			'Masyvai',
			'Ciklai',
			'Sąlygos kintamasis',
			'GET ir POST duomenys',
			'Rašymas ir skaitymas į failus'
		],
		'etapai' => [
			'Susikurti dvimatį masyvą, kuriame būtų laikoma informacija apie pranešimo autorių ir jo turinį (sukurti 3-5 pavyzdinius komentarus), bei juos visus išvesti.',
			'Susikurti formą su dviem laukais: autorius ir turinys. Duomenys siunčiami per POST. Užsubmitinus, duomenys turi būti įrašomi į tekstinį failą CSV formatu.',
			'Pirmame etape ranka parašytą masyvą sugeneruoti iš CSV duomenų tekstiniame faile.',
			'<i>Papildomai: Padaryti taip, kad abu laukai būtų privalomi. Jei kuris nors laukas nėra užpildytas, reikia parodyti klaidos pranešimą.</i>'
		],
		'naudingi' => [
			[
				'content' => '<a href="http://www.phpknowhow.com/get-and-post/understanding-get-and-post/" target="_blank">Apie GET ir POST</a>'
			],
			[
				'content' => '<a href="http://stackoverflow.com/questions/9139202/how-to-parse-a-csv-file-using-php" target="_blank">CSV failo skaitymas</a>'
			],
			[
				'content' => '<a href="https://www.virendrachandak.com/techtalk/creating-csv-file-using-php-and-mysql/" target="_blank">Darbas su CSV failais</a>'
			]
		]
	],
	'todo' => [
		'pavadinimas' => 'To Do sąrašas',
		'aprasymas' => 'To Do, iš anglų kalbos išvertus, reiškia "padaryti". Susidaromas darbų sąrašas, kad nieko nepamiršti ir žinoti iki kada tai turi būti padaryta.',
		'pvz' => 'todo',
		'zinios' => [
			'Kintamieji',
			'Masyvai',
			'Ciklai',
			'Sąlygos kintamasis',
			'Darbas su laiku ir datom',
			'GET ir POST duomenys',
			'Rašymas ir skaitymas į failus',
			'Failų įterpimas (include/require)'
		],
		'etapai' => [
			'Susikurti csv failą, kuris laikytų duomenis apie užduotį: pavadinimas, aprašymas, deadline, prioritetas (aukštas, vidutinis, žemas), būsena (atlikta, neatlikta)',
			'Padaryti užduočių sąrašo išvedimą',
			'Naujos užduoties kūrimas. T.y. forma, kuria duomenys perduodami POST metodu ir jie išsaugomi faile',
			'Užduoties pašalinimas',
			'Užduoties statuso keitimas (pažymėti kaip atliktą). Tai turi atsispindėti užduočių sąraše (pvz išbrauktas)',
			'Užduoties, kuri vėluojama atlikti, turi būti pažymėta sąraše (pvz raudonu fonu)',
			'<i>Papildomai: užduočių rikiavimas. Padaryti taip, užduotys būtų rikiuojamos pagal prioritetą ir, jei vienodo prioriteto, pagal datą (anksčiausiai turintys būti padaryti eina aukščiau).</i>'
		],
		'naudingi' => [
			[
				'content' => '<a href="http://www.phpknowhow.com/get-and-post/understanding-get-and-post/" target="_blank">Apie GET ir POST</a>'
			],
			[
				'content' => '<a href="http://stackoverflow.com/questions/9139202/how-to-parse-a-csv-file-using-php" target="_blank">CSV failo skaitymas</a>'
			],
			[
				'content' => '<a href="https://www.virendrachandak.com/techtalk/creating-csv-file-using-php-and-mysql/" target="_blank">Darbas su CSV failais</a>'
			],
			[
				'content' => '<a href="http://php.net/mktime" target="_blank">mktime() funkcija</a>'
			],
			[
				'content' => '<a href="http://php.net/time" target="_blank">time() funkcija</a>'
			]
		]
	],
	'simple-select' => [
		'pavadinimas' => 'Paprastos SELECT užklausos',
		'pvz' => 'simple-select/atsakymas.png',
		'aprasymas' => 'Labai svarbu gebėti iš duomenų bazės pasiimti tuos duomenis, kurių reikia. Tam naudojamos'
		               .' SELECT užklausos. Užduoties tikslas: sužinoti visų biurų adresus, esančius duomenų bazėje,.'
		               .' surikiuotus pagal geografinę vietą.',
		'etapai' => [
			'Parsisiųsti duombazę iš <a href="/simple-select/database.sql" download="database.sql">čia</a> ir importuoti ją per phpMyAdmin',
			'Parašyti užklausą, kuri iš <i>offices</i> lentelės paimtų visą informaciją apie biuro lokaciją (laukai <i>addressLine1</i>, <i>addressLine2</i>, <i>city</i>, <i>country</i>, <i>state</i>)',
			'Patobulinti prieš tai parašytą užklausą, kad duomenys būtų surikiuoti pagal valstybę (laukas <i>country</i>) mažėjimo tvarka (Z-A)',
			'Patobulinti prieš tai parašytą užklausą, kad jei valstybės yra vienodos, tuomet būtų rikiuojama pagal valstiją (laukas <i>state</i>) didėjimo tvarka (A-Z)'
		],
		'zinios' => [
			'Duomenų bazės importavimas per phpMyAdmin',
			'SELECT užklausos pradmenys',
			'Duomenų rikiavimas pasinaudojant ORDER BY'
		],
		'naudingi' => [
			[
				'content' => '<a href="https://dev.mysql.com/doc/refman/5.7/en/select.html" target="_blank">SELECT oficiali dokumentacija</a>'
			],
			[
				'content' => '<a href="https://www.w3schools.com/php/php_mysql_select.asp" target="_blank">SELECT aprašymas 3WSchools</a>'
			]
		]
	],
	'joins' => [
		'pavadinimas' => 'Duomenų iš skirtingų lentelių jungimas JOIN\'ais',
		'aprasymas' => 'Dažniausiai lentelės yra susijusios. Tarkim yra vartotojai, kurie rašo blogo įrašus. Taigi blogo įrašų lentelė yra susijusi su'
		               .' vartotojų lentele. Tarkim, norėdami sužinoti kiek blog postų yra parašęs kiekvienas iš vartotojų, turite apjungti lenteles.'
		               .' <i>P.S. Užduoties etapai (išskyrus pirmąjį) atstoja skirtingas užklausas.</i>',
		'etapai' => [
			'Parsisiųsti duombazę iš <a href="/simple-select/database.sql" download="database.sql">čia</a> ir importuoti ją per phpMyAdmin',
			'Parašyti užklausą, kuri apjungtų produktų (lentelė <i>products</i>) ir linijų (lentelė <i>productlines</i>)'
			 .' lenteles ir parinktų produkto pavadinimą (laukas <i>productName</i>), mąstelį (laukas <i>productScale</i>),'
			 .' produkto linijos pavadinimą (laukas <i>productLine</i>) ir linijos aprašymą (laukas <i>textDescription</i>)',
		],
		'naudingi' => [
			[
				'content' => '<a href="https://dev.mysql.com/doc/refman/5.7/en/join.html" target="_blank">Oficiali dokumentacija</a>'
			],
			[
				'content' => '<a href="https://www.w3schools.com/sql/sql_join.asp" target="_blank">3WSchools apie JOIN</a>'
			],
			[
				'content' => '<a href="https://www.codeproject.com/KB/database/Visual_SQL_Joins/Visual_SQL_JOINS_orig.jpg" target="_blank">Skirtingų JOIN variacijos grafiškai</a>'
			]
		]
	],
	'select-with-where' => [
		'pavadinimas' => 'Duomenų filtravimas SELECT užklausose',
		'aprasymas' => 'Labai naudingas ir dažnai naudojamas dalykas yra duomenų filtravimas. Kokia nauda iš duombazės, jei jūs greitai negalit sužinot,'
		               .' kurie produktai yra labiausiai perkamiausi arba kurių kaina didesnė nei 100 pinigų? :) Tam pasitelkiama WHERE užklausos dalis.'
		               .' Visi užduoties etapai (išskyrus pirmąjį) reprezentuoja atskirą užklausą.',
		'etapai' => [
			'Parsisiųsti duombazę iš <a href="/simple-select/database.sql" download="database.sql">čia</a> ir importuoti ją per phpMyAdmin',
			'Parašyti užklausą, kuri iš <i>products</i> lentelės paimtų produkto kodą (laukas <i>productCode</i>) ir '
			  .'pavadinimą (laukas <i>productName</i>), tačiau atrinktų tuos, kurių linija (laukas <i>productLine</i>) '
			  .'yra lėktuvas (Plane). Rezultatą surikiuoti pagal gamintoją (laukas <i>productVendor</i>) mažėjimo tvarka '
			  .'(Z-A) ir kiekį (laukas <i>quantityInStock</i>) mažėjimo tvarka (Z-A)',
			'Parašyti užklausą, kuri iš <i>products</i> lentelės paimtų produkto kodą (laukas <i>productCode</i>), '
			  .'pavadinimą (laukas <i>productCode</i>), mąstelį (laukas <i>productScale</i>) ir kiekį (laukas <i>quantityInStock</i>). '
			  .'Atrinkti duomenis, kurių mąstelis yra 1:10 arba 1:18. Duomenis išrikiuoti pagal kiekį mažėjimo tvarka (Z-A)',
			'Parašyti užklausą, kuri iš <i>products</i> lentelės paimtų produkto pavadinima (laukas <i>productName</i>), '
		      .'gamintoją (laukas <i>productVendor</i>), gamintojo siūlomą kainą (laukas <i>MSRP</i>). Atrinkti tik tuos, kurių '
			  .'gamintojo siūloma kaina yra nemažesnė nei 132. Išrikiuoti pagal produkto pavadinimą mažėjančia tvarka (A-Z).',
			'Parašyti užklausą, kuri iš <i>products</i> lentelės paimtų produkto kodą (laukas <i>productCode</i>), pavadinimą '
			  .'(laukas <i>productName</i>) ir pirkimo kainą (laukas <i>buyPrice</i>). Atrinkti tik tuos produktus, kurių pirkimo '
			  .'kaina yra tarp 60 ir 90 (įskaitytinai). Surikiuoti pagal pirkimo kainą didėjimo tvarka (A-Z).'
		],
		'naudingi' => [
			[
				'content' => '<a href="https://dev.mysql.com/doc/refman/5.7/en/select.html" target="_blank">SELECT oficiali dokumentacija</a>'
			],
			[
				'content' => '<a href="https://www.tutorialspoint.com/mysql/mysql-where-clause.htm" target="_blank">Pamoka apie WHERE</a>'
			]
		]
	],
	'say-my-name' => [
		'pavadinimas' => 'Koks tavo vardas?',
		'aprasymas' => 'Užduotis skirta pramankštinti žinias apie $_POST ir formas. Tiesiog reikia vartotojui pasakyti, koks yra jo vardas ir programa turi su juo pasisveikinti.',
		'pvz' => 'say-my-name.php',
		'etapai' => [
			'Sukurti formą su vienu inputu: name',
			'Sukurti formos handler\'į, kuris išveda pasisveikinimą su formoje įvesta reikšme'
		],
		'naudingi' => [
			[
				'content' => '<a href="https://www.w3schools.com/php/php_forms.asp" target="_blank">Kaip elgtis su formom (eng)</a>'
			]
		]
	]
];